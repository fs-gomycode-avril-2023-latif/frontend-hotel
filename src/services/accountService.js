import { isTokenValid } from "../utils/AxiosConfig/verifToken";
import Axios from "./callerService";

let login = (userData) => {
  return Axios.put("api/auth/login", userData);
};
let saveToken = (token) => {
  localStorage.setItem("token", token);
};

let logout = () => {
  localStorage.removeItem("token");
};

let islogged = () => {
  let token = localStorage.getItem("token");
  let isvalid = isTokenValid(token);
  console.log(!!token);
  console.log(isvalid);
  if (!!token === true && isvalid === true) {
    return true;
  }
  return false;
};
const getToken = ()=>{
  return localStorage.getItem("token")
}
export const accountService = {
  login,
  saveToken,
  islogged,
  logout,
  getToken
};
