export const isTokenValid = (token) => {
  if (!token) {
    // Le token est inexistant
    return false
  }
  // Divisez le token en trois parties : en-tête, payload et signature
  const tokenParts = token.split(".");
  if (tokenParts.length !== 3) {
    // Le token n'a pas le format attendu
    return false
  }

  try {
    // Décodez la partie payload (2ème partie) du token
    const payload = JSON.parse(atob(tokenParts[1]));

    // Vérifiez si la date d'expiration est présente et si elle est dans le futur
    if (payload.exp) {
      const expirationTime = payload.exp * 1000; // Convertissez en millisecondes
      const currentTime = Date.now();
      if (expirationTime <= currentTime) {
        // Le token a expiré
        return false
      }
    }

    // Vous pouvez également vérifier la signature ici si vous avez la clé publique ou la clé secrète

    return true; // Le token est valide
  } catch (error) {
    console.log(error);
    return false; // Une erreur s'est produite lors de la vérification
  }
};

