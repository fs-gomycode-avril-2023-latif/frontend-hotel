import axios from "axios";
import { accountService } from "../../services/accountService";

const requestInterceptor = axios.interceptors.request.use(
  (config) => {
    // Vérifie si l'utilisateur est connecté
    if (accountService.islogged()) {
      // Ajoute l'en-tête d'authentification avec le token JWT
      config.headers.Authorization = `Bearer ${accountService.getToken()}`;
    }
    return config;
  },
  (error) => {
    // Gestion des erreurs de la requête
    return Promise.reject(error);
  }
);

export default requestInterceptor;
