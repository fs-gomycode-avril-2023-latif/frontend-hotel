import { configureStore } from "@reduxjs/toolkit";
import authReducer from "./auth/authSlice";
import categoryReducer from "./categories/categorySlice";
import serviceReducer from "./service-chambre/serviceChambreSlice";
import chambreRouter from "./chambres/chambreSlice";
import userRouter from "./users/userSlice";

const store = configureStore({
  reducer: {
    auth: authReducer,
    category : categoryReducer,
    service : serviceReducer,
    chambre : chambreRouter,
    user : userRouter
  },
  devTools: true,
});

export default store;
