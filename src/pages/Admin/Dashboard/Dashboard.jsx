import React from "react";
import { BsArrowDownRight } from "react-icons/bs";
import Chart from "../../../components/admin/data/Chart";

const Dashboard = () => {
  
  return (
    <div>
      <div className="alert alert-secondary fw-bold text-danger text-center">
        Ce sont des données factices
      </div>
      <h3 className="mb-4">Dashboard</h3>
      <div className="d-flex justify-content-between align-items-center gap-3">
        <div className="d-flex justify-content-between align-items-end flex-grow-1 bg-white p-3 rounded-3">
          <div>
            <p>Total</p>
            <h4 className="mb-0">1000€</h4>
          </div>
          <div className="d-flex flex-column align-items-end">
            <h6>
              <span className="text-success">
                <BsArrowDownRight /> 32%
              </span>{" "}
            </h6>
            <p className="mb-0">Compare to April</p>
          </div>
        </div>
        <div className="d-flex justify-content-between align-items-end flex-grow-1 bg-white p-3 rounded-3">
          <div>
            <p>Total</p>
            <h4 className="mb-0">1000€</h4>
          </div>
          <div className="d-flex flex-column align-items-end">
            <h6>
              {" "}
              <span className="text-danger">
                <BsArrowDownRight />
                32%
              </span>
            </h6>
            <p className="mb-0">Compare to April</p>
          </div>
        </div>
        <div className="d-flex justify-content-between align-items-end flex-grow-1 bg-white p-3 rounded-3">
          <div>
            <p>Total</p>
            <h4 className="mb-0">1000€</h4>
          </div>
          <div className="d-flex flex-column align-items-end">
            <h6>
              <span className="text-warning">
                {" "}
                <BsArrowDownRight /> 32%
              </span>{" "}
            </h6>
            <p className="mb-0">Compare to April</p>
          </div>
        </div>
      </div>

      <div className="mt-4">
        <h4 className="mb-4">Icome statics</h4>
        <div>
          <Chart />
        </div>
      </div>
    </div>
  );
};

export default Dashboard;
