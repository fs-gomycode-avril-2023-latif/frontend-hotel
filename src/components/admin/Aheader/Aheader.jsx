import React, { useState } from "react";
import {
  AiOutlinePicRight,
  AiOutlinePicLeft,
  AiTwotoneBell,
} from "react-icons/ai";

import { DownOutlined } from "@ant-design/icons";

import { Layout, Button, theme } from "antd";
import { Dropdown, Space } from "antd";
import { Link } from "react-router-dom";
import avatar from "../../../assets/images/users/avatar1.png";
const { Header } = Layout;

const items = [
  {
    label: <Link href="https://www.antgroup.com">1st menu item</Link>,
    key: "0",
  },
  {
    label: <Link href="https://www.aliyun.com">2nd menu item</Link>,
    key: "1",
  },
  {
    type: "divider",
  },
  {
    label: "3rd menu item",
    key: "3",
  },
];

const Aheader = () => {
  const [collapsed, setCollapsed] = useState(false);
  const {
    token: { colorBgContainer },
  } = theme.useToken();
  const user = {};
  return (
    <Header
      className="d-flex justify-content-between ps-1 pe-5"
      style={{
        padding: 0,
        background: colorBgContainer,
      }}
    >
      <Button
        type="text"
        icon={collapsed ? <AiOutlinePicRight /> : <AiOutlinePicLeft />}
        onClick={() => setCollapsed(!collapsed)}
        style={{
          fontSize: "16px",
          width: 64,
          height: 64,
        }}
      />
      <div className="d-flex gap-2 align-items-center">
        <div>
          <Dropdown
            menu={{
              items,
            }}
            trigger={["click"]}
          >
            <Link onClick={(e) => e.preventDefault()}>
              <Space>
                <div className="position-relative text-dark">
                  <AiTwotoneBell className="fs-5" />
                  <DownOutlined />
                  <span className="badge bg-warning rounded-circle p-1 position-absolute">
                    4
                  </span>
                </div>
              </Space>
            </Link>
          </Dropdown>
        </div>

        <div className="d-flex gap-3 align-items-center">
          <div>
            <img src={avatar} alt=" user" width={30} height={30} />
          </div>
          <div className="d-flex flex-column justify-content-center">
              <h5 className="m-0 text-dark">
                prenom <i>nom</i>
              </h5>
              <p className="m-0 text-secondary lead">email</p>
            </div>
        </div>
      </div>
    </Header>
  );
};

export default Aheader;
