import React from "react";
import "./footer.css";
import { Link } from "react-router-dom";
const Footer = () => {
  return (
    <footer className="bg-dark text-light text-center py-1 ">
      <div className="container">
        <div className="row">
          <div className="col-md-9  ">
            <h4>Informations de contact</h4>
            <div className="d-flex align-items-center gap-2">
              <p>Adresse : Angre Djorogobite 1 |</p>
              <p>Téléphone : +225 05 64 685 613 |</p>
              <p>Email : Juniorgneba@gmail.com</p>
            </div>
          </div>
          <div className="col-md-3">
            <h4>Liens utiles</h4>
            <ul className="list-unstyled d-flex gap-2 align-items center">
              <li>
                <Link className="nav-link" to="/">
                  Accueil
                </Link>
              </li>
              <li>
                <Link className="nav-link" to="/service">
                  Services
                </Link>
              </li>
              <li>
                <Link className="nav-link" to="/contact">
                  Contact
                </Link>
              </li>
            </ul>
          </div>
        </div>
        <hr className="my-2" />
        <p>
          &copy; {new Date().getFullYear()} Latif Entreprise. Tous droits
          réservés.
        </p>
      </div>
    </footer>
  );
};

export default Footer;
