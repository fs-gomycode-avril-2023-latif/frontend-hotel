import React, { useState } from 'react';
import { Button, Input, Space } from 'antd';
import { SearchOutlined, RedoOutlined } from '@ant-design/icons';

const Filtre = ({ onSearch, onReset }) => {
  const [searchText, setSearchText] = useState('');

  const handleSearch = () => {
    onSearch(searchText);
  };

  const handleReset = () => {
    setSearchText('');
    onReset();
  };

  return (
    <div className="category-filter">
      <Input
        placeholder="Rechercher une catégorie"
        value={searchText}
        onChange={(e) => setSearchText(e.target.value)}
      />
      <Space>
        <Button type="primary" onClick={handleSearch} icon={<SearchOutlined />} />
        <Button onClick={handleReset} icon={<RedoOutlined />} />
      </Space>
    </div>
  );
};

export default Filtre;
