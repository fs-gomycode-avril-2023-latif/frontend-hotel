import React from "react";
import "./App.css";
import PublicRouter from "./pages/Public/PublicRouter";
import AdminRouter from "./pages/Admin/AdminRouter";
import { Route, Routes } from "react-router-dom";
import AuthRouter from "./pages/Auth/AuthRouter";
import AuthGuard from "./helpers/AuthGuard";

const App = () => {
  return (
    <>
      <Routes>
        <Route path="/*" element={<PublicRouter />}></Route>
        <Route
          path="/admin/*"
          element={
            <AuthGuard>
              <AdminRouter />
            </AuthGuard>
          }
        />
        <Route path="/auth/*" element={<AuthRouter />}></Route>
      </Routes>
    </>
  );
};

export default App;
